from django.db import models


# Create your models here.
class Question(models.Model):
    def __str__(self):
        return f"{self.pub_date.strftime('%Y-%m-%d %H-%M-%S')} "\
        f"- {self.q_text}"
    q_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField()


class Choice(models.Model):
    def __str__(self):
        return f"{self.question.q_text} {self.choice_text} - {(self.votes)}"
    question = models.ForeignKey(
        Question, 
        on_delete=models.CASCADE, 
        related_name="choices"
    )

    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)
