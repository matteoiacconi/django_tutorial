from django.urls import path
from . import views

urlpatterns = [

path('', views.QuestionsListView.as_view(), name='questions_list'),

path("question/<int:pk>/", views.QuestionDetailView.as_view(), name="question_detail")
]