from django.shortcuts import render
from django.views import generic
from .models import Question
# Create your views here.

class QuestionsListView(generic.ListView):
    model = Question
    template_name = 'polls/questions.html'

class QuestionDetailView(generic.DetailView):
    model = Question
    template_name = "polls/question_detail.html"
    